# ScoreCard
企业/个人信用评分卡构建


## 数据
- 企业基础信息以及企业交易数据：client_data，企业数据需人工校验
- 企业账期订单记录数据：loan_list
- 中间数据
   - 训连好的模型：model
   - 用于计算模型PSI的验证数据集：valid
3. 人工核对好的企业基本信息数据表：feature_checked.xlsx


## ipython
项目过程代码文件

## Python

**1. step1_feature_generate**
- 此代码模块包括了样本所有特征的预处理和计算代码：
  - feature_company_info： 样本企业基础信息变量整理
  - feature_season_trade： 提取样本季度交易数据变量
  - feature_season_loan： 提取样本季度逾期情况变量
  - feature_history_trade： 提取样本历史交易数据变量
  - feature_history_loan： 提取样本历史逾期情况变量
  - 注：在提取变量之前，需要对数据的准确性进行人工核查，并对字段取值进行统

**2.step2_feature_analysis**
- 此代码块包含了样本候选特征的探索性统计分析代码：
  - company_loan_statistic： 分时间段统计了客户所有账期订单的逾期情况
  - statistic_feature： 统计了所有候选变量的空值率，取值分布，IV值，与label的相关性
  - feature_correlation： 统计了候选变量间的相关性，并筛选出了相关性大于0.8的变量对
  - pca,fa: 分别是采用主成分分析（PCA）和因子分析（FA）方法对所有候选变量间的多重共线性进行分析
  - discrete_feature： 统计了离散变量所有取值类别对应的样本逾期情况
  - bins_num,bins_value： 对连续变量进行了等量和等距分段，并统计变量每个取值分段对应的样本逾期情况
  
**3.step3_mdoel_validation**
- 此代码块包含最优模型寻找过程中的所有备选模型训练代码：
  - feature_pretreatment_value： 变量预处理方式一
  - feature_pretreatment_woe： 变量预处理方式二（适用于评分卡构建模型）
  - model_GBDT： 梯度提升决策树GBDT模型训练代码，调用xgboost工具包
  - model_SVM： 支持向量机SVM模型训练代码,调用sklearn机器学习工具包
  - model_NB： 朴素贝叶斯Naive Bayes模型训练代码,调用sklearn机器学习工具包
  - model_KNN： K近邻KNN模型训练代码,调用sklearn机器学习工具包
  - model_NN： 神经网络Neural Network模型训练代码,调用sklearn机器学习工具包
  - model_DT： 决策树DT模型训练代码,调用sklearn机器学习工具包
  - model_LR： 逻辑回归Logistic Regression模型训练代码,调用sklearn机器学习工具包
  - ks_value： 模型KS统计量计算代码
  - psi_value： 模型PSI值计算代码
  - model_train： 所有模型训练代码
  - GBDT_CV： 梯度提升决策树GBDT模型5折交叉验证代码
  - model_CV： 其他模型5折交叉验证代码
  
**4. step4_build_ScoreCard**
- 此代码块包含模型转信用分值的代码：
  - score_card： 输出所有样本的违约概率与信用分值
  - calc_score： 采用概率公式直接计算样本信用分值
  - calc_score_by_card： 采用评分卡方式计算信用分值
  - bin_statistic: 所有样本的信用分值分组统计
 
 ps：采用概率公式和采用评分卡方式计算得出的样本信用分值相同
  
**5.Functions**
此代码快包含了所有公共函数
  - 变量值计算函数
  - 变量统计分析函数
  - 变量预处理函数

注：代码中包含了每个函数的功能说明以及参数和返回值说明
